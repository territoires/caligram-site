---
layout: post
title:  "Une gestion horizontale, radicalement ouverte et efficace"
author: Simon
---

La coopérative Caligram est particulièrement fière de son mode de gestion horizontal et radicalement ouvert. Nous avons documenté l'ensemble des processus de la coopérative qui nous permettent de tendre vers l'inclusivité, la diversité et l'efficacité ici : [Comment ça marche la coopérative Caligram?](http://commentcamarche.caligram.com) 

![Capture d'écran de nos processus](/assets/images/blog/commentcamarche.png#wide)

À lire si vous êtes nerds de gouvernance 🤓. Commentaires bienvenus ! 