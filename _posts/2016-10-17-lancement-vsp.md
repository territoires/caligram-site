---
layout: post
title:  "Lancement du calendrier culturel de l’arrondissement Villeray–St-Michel–Parc-Extension"
author: Solen
---

Une nouvelle saison culturelle et numérique s’annonce pour l'arrondissement de Villeray–Saint-Michel–Parc-Extension&nbsp;! Ça y est, c’est officiel, le premier calendrier culturel Web propulsé par [Caligram](http://pro.caligram.com) est lancé&nbsp;: [culturevsp.com](http://culturevsp.com/)

Territoires, coopérative numérique, est fière d’être le partenaire de l'arrondissement de Villeray–Saint-Michel–Parc-Extension en accomplissant ce défi&nbsp;: concevoir un calendrier culturel VSP ludique, simple et incontournable pour profiter de l’ensemble de la programmation culturelle de l’arrondissement. Toute l’équipe a travaillé fort pour rendre l’interface agréable et facile d’accès. Elle offre aux internautes une information évènementielle bien structurée. «&nbsp;Je cherche, je trouve&nbsp;» : il suffit de quelques clics pour découvrir l’actualité, des extraits de spectacles, les dates de conférences, par date, mois, lieu… Pratique, non&nbsp;?

Territoires souhaite que cette belle collaboration ouvre la voie à de nouveaux projets propulsés par Caligram, service de calendriers sur mesure pour les organisations et les collectivités&nbsp;!
