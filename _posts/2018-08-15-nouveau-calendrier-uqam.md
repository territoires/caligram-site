---
layout: post
title:  "Un nouveau calendrier pour l’UQAM"
author: Étienne
---

Nous sommes très fières et fiers d’accueillir sur Caligram le nouveau [calendrier des événements](https://evenements.uqam.ca) de l’Université du Québec à Montréal!

La mise en ligne de ce calendrier représente un jalon important pour notre coopérative. Avec ses plus de 250 groupes, l’UQAM représente un nouveau sommet pour notre plateforme en termes d’échelle et de complexité.

![Captures d’écran du nouveau calendrier de l’UQAM](/assets/images/blog/nouveau-calendrier-uqam.jpg#wide)

Nous avons apporté tout un lot d’améliorations à Caligram afin de répondre adéquatement à ce défi&nbsp;– et bien sûr, c'est l’ensemble des membres de notre plateforme qui en bénéficie.

Un merci particulier aux équipes du [Service des communications](https://servicecom.uqam.ca) et du [Service de l’audiovisuel](http://www.audiovisuel.uqam.ca), avec qui nous avons collaboré étroitement afin que la transition se fasse en douceur et que le calendrier respecte les normes visuelles de l’UQAM.
