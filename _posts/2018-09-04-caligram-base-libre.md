---
layout: post
title:  "Nouveau pas vers le logiciel libre"
author: Étienne
---

[caligram-base](https://gitlab.com/territoires/calendriers/caligram-base) est l’outil que nous utilisons à l’interne pour créer tous nos calendriers (comme [Culture VSP](http://culturevsp.com) et le [Calendrier de l’UQAM](https://evenements.uqam.ca)). Nous sommes très heureux et heureuses d’en partager aujourd’hui le [code](https://gitlab.com/territoires/calendriers/caligram-base) sur GitLab sous licence BSD.

Quelques détails techniques&nbsp;: il s’agit d’une application JavaScript universelle (client et serveur) basée sur React et Redux. Elle se connecte à notre [API](http://api.caligram.com) pour afficher les événements d’un calendrier Caligram.

Ceci s’inscrit dans notre stratégie de passage au logiciel libre [annoncée plus tôt](2018-07-18-en-route-vers-le-libre). On a bien hâte de compléter la transition!