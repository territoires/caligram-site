---
layout: post
title:  "Caligram déménage!"
author: Diane
---

Le printemps et le début de l’été sont traditionnellement synonymes de déménagement au Québec. La coopérative Caligram, spécialisée dans les calendriers numériques, a déplacé ses bureaux pour s’installer avec la coopérative <a href="https://molotov.ca">Molotov</a> au 2065 rue Parthenais à Montréal, en plein cœur du quartier Centre-Sud.

![Devanture de la Grover](/assets/images/blog/grover.jpg)

Caligram se consacre exclusivement à offrir à ses partenaires et collaborateurs des calendriers numériques. La coopérative emploie 13 employé·e·s et offre ses services aux entreprises locales et internationales. Pour obtenir plus de renseignements sur Caligram, <a href="/">c’est par ici</a>.