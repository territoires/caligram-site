---
layout: post
title:  "Un écosystème sophistiqué pour le rayonnement culturel à Laval"
author: Simon
---

Nous sommes très fières et fiers d’accueillir sur Caligram le nouveau [calendrier Signé Laval](https://signelaval.com/fr/evenements) réalisé pour le [Conseil régional de la culture de Laval](https://http://crclaval.com/) en partenariat étroit avec [Folklore](https://folkloreinc.ca/fr) et [Culture Creates](http://www.culturecreates.com/fr/).

C'est toute l'information culturelle de Laval qui s'y retrouve et qui peut être réutilisée sur les sites de l'ensemble des organisations participantes.

![Capture d'écran de signé Laval](/assets/images/blog/signelaval.jpg#wide)

Nous avons apporté tout un lot d’améliorations à Caligram afin de répondre adéquatement à ce défi&nbsp;– et bien sûr, c'est l’ensemble des membres de notre plateforme qui en bénéficie.