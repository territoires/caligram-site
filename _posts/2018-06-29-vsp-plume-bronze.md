---
layout: post
title:  "Un prix pour le Calendrier culturel VSP!"
author: Étienne
---

Le projet de [calendrier culturel](http://culturevsp.com) de l’arrondissement Villeray–St-Michel–Parc-Extension a remporté un prix au Gala Plumes d'Excellence 2018 de l’[Association des communicateurs municipaux du Québec](http://acmq.qc.ca)! Il s’agit de la Plume de Bronze dans la catégorie «&nbsp;Nouveaux médias&nbsp;– 100&nbsp;000 citoyens et plus&nbsp;».

![Audrey Villeneuve et Danielle Lamarre Trignac reçoivent la Plume de Bronze au Gala Plumes d'Excellence 2018](/assets/images/blog/vsp-plume-bronze.jpg)
<p class="caption">Source: <a href="https://www.facebook.com/ACMQ1978/photos/ms.c.eJxFkckNxAAIAztacZmj~;8ZWgUC~_I2OM4aRg0pBSVQ7~;8QtI3B0sB3gUsaACVZWcB7IVoBcY16OIWlMjb4WtqemM8I3YbKHdYvD2QL0AlE8wDTtFe1CdB6pB7oi5DbgRn2C2wcDaCsUqQp8tRucRk0Mvh3RjJOchPuCOS2rgWxC0O1WsAjbAPqDPFo~;PY4L5BasB~_AA6mNz58wb63iD9qMg80B4suE4xncaBKYjuc9oKs~;gD6v9xHw~-~-.bps.a.1807103422666540.1073741836.254210071289224/1807104649333084/?type=3&theater">Page Facebook de l’ACMQ</a></p>

Nous sommes très fiers d’avoir collaboré sur ce projet avec l’équipe de l’arrondissement, particulièrement Audrey Villeneuve et Danielle Lamarre Trignac (ci-dessus). Bravo, et au plaisir de poursuivre cette belle collaboration!