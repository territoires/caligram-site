---
layout: post
title:  "Vers le logiciel libre"
author: Étienne et Robin
---

Caligram deviendra un logiciel libre! C’est un projet que nous envisagions depuis plusieurs années, et nous sommes fières et fiers d’annoncer que toutes les composantes de notre plateforme seront disponibles sous licence libre d’ici la fin de l’année.

## Qu’est-ce qu’un logiciel libre?

Un logiciel libre est un logiciel qu’on peut utiliser, copier, distribuer, étudier, modifier et améliorer, par opposition à un logiciel non-libre (ou propriétaire) qu’on peut généralement seulement utiliser.

## Pourquoi?
1. **C’est en accord avec nos valeurs.** Nous souhaitons que l’ensemble de Caligram, pas seulement ses données, devienne un bien commun, une ressource collective et nous inscrire davantage comme plateforme coopérative dans l’économie sociale et numérique
2. **Ça pogne.** C’est attrayant pour les clients qui partagent nos valeurs, notamment les institutions publiques et parapubliques qui s’intéressent de plus en plus au logiciel libre (voir l’[annonce récente de la Ville de Montréal](https://beta.montreal.ca/nouvelles/nouvelle-politique-au-service-de-linnovation-numerique) à ce sujet). C’est un argument supplémentaire à l’angle «approvisionnement responsable».
3. **C’est sécurisant.** Si on décide un jour de fermer la coop et partir à Punta Cana, les gens qui utilisent notre logiciel propriétaire sont mal pris. L’accès au code devient un argument important et rassurant. (Mais non, on partira pas à Punta Cana.)
4. **Mieux vaut être les premiers.** Une plateforme comparable à Caligram n’existe pas ailleurs, mais elle pourrait. Si une alternative libre apparaîssait avant nous, notre plateforme serait moins attrayante; mieux vaut nous positionner comme la plateforme libre de référence.
5. **La compétition gratuite existe déjà.** Le fait que le code de Caligram devienne gratuit ne nous empêche pas de faire des affaires; l’alternative la plus commune à Caligram est probablement une extension WordPress gratuite, et pourtant on existe.
6. **Notre valeur est ailleurs.** Le logiciel lui-même n’est pas ce qu’on vend principalement ni ce qui nous distingue le plus. Notre avantage et notre valeur sont ailleurs :
  * Notre réseau collectif d’événements;
  * Notre expertise dans les calendriers;
  * Nos services de personnalisation sur mesure;
  * Le soutien et les mises à jour constantes;
  * L’hébergement dans l’infocumulonimbus (terme technique).
7. **Notre code n’est pas magique.** Soyons honnêtes, Caligram n’est pas une technologie ultra-secrète avec des algorithmes du futur. Même sans accès à notre code source, d’autres pourraient faire leur propre clone bas de gamme avec relativement peu d’efforts (et peut-être le rendre gratuit et libre, voir point&nbsp;4).
8. **La compétition peut nous profiter.** Si on choisit la bonne licence, d’autres équipes qui reprendraient notre code et offriraient leur propre service parallèle seraient dans l’obligation de partager leurs améliorations, ce dont nous profiterions directement.
9. **On conserve l’avantage du nom et de l’expérience.** On s’appelle Caligram, et on a un service déjà bien établi qui roule depuis plus longtemps que n’importe quelle compétition potentielle.

## Notre plan

Notre plateforme est constituée de 4 composantes. Nous allons les libérer progressivement au cours des prochains mois.

### [caligram-react](https://gitlab.com/territoires/caligram-react) – déjà disponible
Notre boîte à outils de composantes React à partir de laquelle on construit des interfaces de calendriers.

### [caligram-base](https://gitlab.com/territoires/calendriers/caligram-base) – ~~juillet 2018~~ septembre 2018
Le calendrier de base sur lequel tous nos calendriers sont basés. Avec une clé d’API et quelques heures de travail, on obtient un calendrier sur mesure (qui n’est cependant pas utilisable indépendamment de l’API et de l’interface d’admin).

### caligram-api – décembre 2018
Le cœur de Caligram. C’est le morceau de code avec lequel tous les autres morceaux communiquent. Avec un accès au code de l’API, on peut mettre sur pied sa propre instance de Caligram, indépendant de caligram.com.

### caligram-admin – décembre 2018
L’interface d’administration qui permet de créer, modifier, supprimer des événements, des organisations, des utilisateurs (et éventuellement des lieux, types, thèmes, publics, etc.).

## Choix de licences
Nous comptons utiliser deux types de licences.

* **AGPLv3** est une licence virale&nbsp;: tout code dérivé doit aussi être distribué avec la même licence. Donc toutes modifications faites par d’autres doivent être rendues disponibles en retour. C’est ce que nous prévoyons utiliser pour **caligram-api** et **caligram-admin**, qui sont le cœur de notre plateforme et pour lesquels nous voulons encourager les améliorations qui demeurent disponibles à la collectivité, mais empêcher les clones propriétaires.
* **BSD 3-clause** est une licence flexible&nbsp;: le code peut être utilisé librement, modifié, mais pas nécessairement redistribué selon les mêmes termes. C’est ce que nous utilisons pour **caligram-react**, et comptons utiliser pour **caligram-base**.

La licence donne à toute personne recevant le logiciel le droit illimité de l’utiliser, le copier, le modifier, le fusionner, le publier, le distribuer, le vendre et de changer sa licence. La seule obligation est de mettre le nom des auteurs avec la notice de copyright.

## Exemples comparables
D’autres entreprises fonctionnent selon un modèle semblable, et dont nous nous sommes inspirés à différents degrés&nbsp;:

* [WordPress](https://wordpress.org) rend son logiciel disponible gratuitement et librement. Ils le rentabilisent avec leur service d’hébergement payant, qui est la façon la plus simple et la plus populaire d’héberger un site WordPress.
* [GitLab](http://gitlab.com) est libre mais pas complètement gratuit. Certaines fonctionnalités sont disponibles en payant pour leur service hébergé, ou en faisant une installation maison qui doit être activée par une clé payante.
* [Discourse](https://www.discourse.org) est libre et gratuit. Ils le rentabilisent avec leur service hébergé, dont le prix est modulé principalement selon le nombre de visites, le nombre d’admins, et l’espace disque utilisé.
* [Loomio](https://www.loomio.org) est libre et gratuit. Ils font leur cash avec leur service d’hébergement, dont le prix est modulé selon le nombre de groupes et de sous-groupes.

*Mis à jour le 4 septembre 2018 pour ajouter un lien vers [caligram-base](https://gitlab.com/territoires/calendriers/caligram-base), maintenant disponible sous licence libre.*