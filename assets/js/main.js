//Skrollr
if ((/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) == false) {
      skrollr.init({forceHeight: false});
}

if( (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) == true ) {
 $('.navbar-default').css({"background-color":"rgba(0, 0, 0, 0.75)"});
}

// Fastclick
$(function() {
    FastClick.attach(document.body);
});